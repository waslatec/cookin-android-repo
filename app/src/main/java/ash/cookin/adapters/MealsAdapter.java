package ash.cookin.adapters;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import ash.cookin.R;
import ash.cookin.fragmentss.home_activity_fragments.ImageViewerFragment;
import ash.cookin.fragmentss.home_activity_fragments.MealDetailsFragment;
import ash.cookin.fragmentss.home_activity_fragments.MealsFragment;
import ash.cookin.models.MealModel;
import butterknife.OnClick;

/**
 * Created by ahmed on 2/21/17.
 */

public class MealsAdapter extends RecyclerView.Adapter<MealsAdapter.ViewHolder> implements View.OnClickListener {

    FragmentActivity activity;
    ArrayList<MealModel> mealModelArrayList;
    ViewHolder holder;
    RecyclerView recyclerView;

    public MealsAdapter() {
    }

    public MealsAdapter(FragmentActivity activity, ArrayList<MealModel> mealModelArrayList, RecyclerView recyclerView) {
        this.activity = activity;
        this.mealModelArrayList = mealModelArrayList;
        this.recyclerView = recyclerView;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.meal_custom, parent, false);
        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    @Override
    public void onClick(View v) {
        try {
            int itemPosition = recyclerView.getChildPosition(v);
            openDetails();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void openDetails(){
        MealDetailsFragment mealDetailsFragment = new MealDetailsFragment();
        activity.getSupportFragmentManager().beginTransaction().addToBackStack("").add(R.id.contentView, mealDetailsFragment).commit();
    }

    private void shareDilog(){
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.share_dialog);
        dialog.setCancelable(true);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void openViewPhotosFragment() {
        ImageViewerFragment imageViewerFragment = new ImageViewerFragment();
        activity.getSupportFragmentManager().beginTransaction().addToBackStack("").add(R.id.contentView, imageViewerFragment).commit();
    }

    @Override
    public int getItemViewType(final int position) {
        return super.getItemViewType(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView shareImageView;
        public ImageView viewPhotosImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            shareImageView = (ImageView) itemView.findViewById(R.id.shareImageView);
            viewPhotosImageView = (ImageView) itemView.findViewById(R.id.viewPhotosImageView);
            shareImageView.setOnClickListener(this);
            viewPhotosImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                switch (v.getId()){
                    case R.id.shareImageView:
                        getAdapterPosition();
                        shareDilog();
                        break;
                    case R.id.viewPhotosImageView:
                        openViewPhotosFragment();
                        break;
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return 30;
    }

    public long getItemId(int position) {
        return position;
    }
}
