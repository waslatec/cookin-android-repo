package ash.cookin.fragmentss.home_activity_fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import ash.cookin.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ahmed on 3/19/17.
 */

public class MealDetailsFragment extends Fragment {
    @BindView(R.id.shareImageView) ImageView shareImageView;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.meal_details_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.shareImageView) void shareImageViewAction() {
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.share_dialog);
        dialog.setCancelable(true);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @OnClick(R.id.proceedButton) void proceedButtonAction() {
        RequestMealFragment requestMealFragment = new RequestMealFragment();
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack("").add(R.id.contentView, requestMealFragment).commit();
    }

    @OnClick(R.id.viewPhotosImageView) void viewPhotosImageViewAction() {
        ImageViewerFragment imageViewerFragment = new ImageViewerFragment();
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack("").add(R.id.contentView, imageViewerFragment).commit();
    }
}
