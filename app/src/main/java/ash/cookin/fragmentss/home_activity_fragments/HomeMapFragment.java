package ash.cookin.fragmentss.home_activity_fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ash.cookin.R;
import ash.cookin.fragmentss.profile_activity_fragments.ForgotPasswordFragment;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ahmed on 3/14/17.
 */

public class HomeMapFragment extends Fragment implements OnMapReadyCallback {

    View view;
    SupportMapFragment mMapFragment;
    FragmentTransaction fragmentTransaction;
    GoogleMap googleMap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_map_fragment, container, false);
        ButterKnife.bind(this, view);
        initialization();
        return view;
    }

    private void initialization(){
        mMapFragment = SupportMapFragment.newInstance();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mapFragment, mMapFragment);
        fragmentTransaction.commit();
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        try {
            LatLng latLngSrc = new LatLng(31.2555, 30.2144);
            googleMap.addMarker(new MarkerOptions()
                    //.icon(BitmapDescriptorFactory.fromResource(R.drawable.cooki_icons))
                    .position(latLngSrc));

            LatLng latLngDes = new LatLng(31.96328, 30.82144);
            googleMap.addMarker(new MarkerOptions()
                    //.icon(BitmapDescriptorFactory.fromResource(R.drawable.cooki_icons))
                    .position(latLngDes));

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngDes, 15.0f));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @OnClick(R.id.filterImageButton) void filterImageButtonAction() {
        loadingDialog();
    }

    protected void loadingDialog() {
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_dialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
}
