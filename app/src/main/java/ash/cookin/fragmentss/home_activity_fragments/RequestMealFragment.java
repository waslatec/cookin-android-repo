package ash.cookin.fragmentss.home_activity_fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ash.cookin.R;

/**
 * Created by ahmed on 3/19/17.
 */

public class RequestMealFragment extends Fragment {

    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.request_meal_fragment, container, false);
        return view;
    }
}