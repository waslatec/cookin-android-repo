package ash.cookin.fragmentss.home_activity_fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ash.cookin.R;
import ash.cookin.adapters.MealsAdapter;
import ash.cookin.models.MealModel;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ahmed on 3/15/17.
 */

public class MealsFragment extends Fragment {
    @BindView(R.id.mealsRecyclerView) RecyclerView mealsRecyclerView;
    View view;
    MealsAdapter mealsAdapter;
    ArrayList<MealModel> mealModelArrayList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.meals_fragment, container, false);
        ButterKnife.bind(this, view);
        initilization();
        set();
        return view;
    }

    private void initilization(){
        mealModelArrayList = new ArrayList<>();
        mealsAdapter = new MealsAdapter(getActivity(), mealModelArrayList, mealsRecyclerView);
    }
    private void set(){
        mealsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mealsRecyclerView.setAdapter(mealsAdapter);
    }
}
