package ash.cookin.fragmentss.profile_activity_fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ash.cookin.R;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ahmed on 3/8/17.
 */

public class SignInFragment extends Fragment {

    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.signin_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.newUserButton) void newUserAction() {
        RegistrationFragment registrationFragment = new RegistrationFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentView, registrationFragment).addToBackStack("").commit();
    }

    @OnClick(R.id.forgotPasswordButton) void forgotPasswordAction() {
        ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentView, forgotPasswordFragment).addToBackStack("").commit();
    }

    @OnClick(R.id.loginButton) void loginButtonAction() {
        ActivationFragment activationFragment = new ActivationFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentView, activationFragment).addToBackStack("").commit();
    }
}