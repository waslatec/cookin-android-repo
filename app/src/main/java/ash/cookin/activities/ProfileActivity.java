package ash.cookin.activities;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import ash.cookin.R;
import ash.cookin.fragmentss.profile_activity_fragments.SignInFragment;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        getSupportActionBar().hide();
        statusBar();
        toSignIn();
    }

    private void statusBar(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getColor(R.color.black));
        }
    }

    private void toSignIn(){
        SignInFragment signInFragment = new SignInFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.contentView, signInFragment).commit();
    }
}
