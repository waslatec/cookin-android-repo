package ash.cookin.activities;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import ash.cookin.R;
import ash.cookin.adapters.MealsAdapter;
import ash.cookin.fragmentss.home_activity_fragments.HomeMapFragment;
import ash.cookin.fragmentss.home_activity_fragments.MealsFragment;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        statusBar();
        getSupportActionBar().hide();
        openMapFragment();
    }

    private void statusBar(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getColor(R.color.black));
        }
    }

    private void openMapFragment(){
        HomeMapFragment homeMapFragment = new HomeMapFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.contentView, homeMapFragment).commit();
    }

    @OnClick(R.id.homeImageView) void homeImageViewAction() {
        MealsFragment mealsFragment = new MealsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.contentView, mealsFragment).addToBackStack("").commit();
    }
}